﻿using System;

namespace Lv3zad3
{
    class Program
    {
        
        //ako ne navedemo loggeru da pise u drugi file on ce pisat u trenutni
        static void Main(string[] args)
        {
            Logger log = Logger.GetInstance();
            log.Logg("hello world");
            Console.Read();
        }
    }
}

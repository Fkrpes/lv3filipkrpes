﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv3zad3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;

       
        private Logger()
        {
            this.filePath = "logg.txt";
        }

        
        
        public static Logger GetInstance()
        {
            if (instance== null)
            {
                
                instance = new Logger();
            }

            return instance;
        }

        public void Logg(string msgText)
        {
           using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(msgText);
            }
        }

        public void SetPath(string newPath)
        {
            filePath = newPath;
        }


    }
}

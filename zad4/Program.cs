﻿using System;

namespace lv3zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager notification_manager = new NotificationManager();
            ConsoleNotification consoleNotification = new ConsoleNotification("Dio", "Its a enemy stand attack", "It is I DIO ZAWORLDO", DateTime.Now, Category.ERROR, ConsoleColor.Yellow);
            notification_manager.Display(consoleNotification);

            consoleNotification = new ConsoleNotification("GyroZeppeli", "Watch out Jonny", "Dio just turned into a T-REX", DateTime.Now, Category.ALERT, ConsoleColor.Magenta);
            notification_manager.Display(consoleNotification);

            consoleNotification = new ConsoleNotification("Jonathan", "Prepares to shoot his nails", "Jonny used the perfect rotation it was very effective", DateTime.Now, Category.INFO, ConsoleColor.Blue);
            notification_manager.Display(consoleNotification);


            Console.Read();
        }
    }
}

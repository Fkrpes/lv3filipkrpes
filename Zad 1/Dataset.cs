﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv3
{
    class Dataset:Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }

        public Prototype Clone()
        {
            List<List<string>> copiedData = new List<List<string>>();
            for (int i = 0; i < data.Count; i++)
            {
                List<string> row = new List<string>();
                foreach (string rowSegment in data[i])
                {
                    row.Add(rowSegment);
                }

                copiedData.Add(row);
            
            }

            Dataset copyOfDataset = new Dataset() ;
            copyOfDataset.data = copiedData;
            return (Prototype)copyOfDataset ;
        }

        public override string ToString()
        {
            StringBuilder toPrint = new StringBuilder();
            foreach (List<string> list in this.data)
            {
                foreach (string segment in list)
                {
                    toPrint.Append(segment);
                }
                toPrint.Append("\n");

            
            
            }
            return toPrint.ToString();
        }

    }
}

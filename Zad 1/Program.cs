﻿using System;

namespace lv3
{
    class Program
    {
        static void Main(string[] args)
        {
            //nije potrebno u ovom slucaju koristiti duboko kopiranje jer sve sto kopiramo su stringovi , a duboko kopiranje koristimo za kopiranje reference tipova podataka
            Dataset csvFile = new Dataset("C:\\Users\\Filip\\source\\repos\\lv3\\lv3\\bin\\Debug\\netcoreapp3.1");

            Dataset clonedcsvFile = new Dataset();
            Console.WriteLine(csvFile.ToString());


            clonedcsvFile = (Dataset)csvFile.Clone();
            Console.WriteLine(clonedcsvFile.ToString());
        }

       
    }
}

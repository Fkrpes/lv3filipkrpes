﻿using System;

namespace Lv3zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Primjer s double tipom podataka bi bio identican stoga ga nisam naveo
            RandomMatrixGenerator matrix_gen =  RandomMatrixGenerator.Getinstance();

            int[,] matrix = new int[6, 9];
            matrix = matrix_gen.GetINTMatrix(6, 9);

            for (int i = 0; i < 6; i++) 
            {
                for (int j = 0; j < 9; j++)
                {
                    Console.Write(String.Format("{0}", matrix[i, j]) + "\t");
                }
                Console.Write("\n");
            }

            //navedena metoda ima 3 odgovornosti kreira matricu, popunjava ju te ju na kraju vrača
            Console.Read();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv3zad2
{
    class RandomMatrixGenerator
    {
        private static RandomMatrixGenerator instance;
        private Random generator;
        private RandomMatrixGenerator()
        {
            this.generator = new Random();
        }

        public static RandomMatrixGenerator Getinstance()
        {
            if (instance == null)
            {
                instance = new RandomMatrixGenerator();
            }
            return instance;

        }

        public int NextInt()
        {
            return this.generator.Next();
        }
        public int NextInt(int upperBound)
        {
            return this.generator.Next(upperBound);
        }
        public int NextInt(int lowerBound, int upperBound)
        {
            return this.generator.Next(lowerBound, upperBound);
        }
        public double NextDouble()
        {
            return this.generator.NextDouble();
        }

        public int[,] GetINTMatrix(int rows, int columns)
        {
            int[,] matrix = new int[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {

                    matrix[i, j] = this.NextInt();



                }
            }

            return matrix;
        }

        public double[,] GetDoubleMatrix(int rows, int columns)
        {
            double[,] matrix = new double[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {

                    


                    matrix[i, j] = this.NextDouble(); 

                }
            }

            return matrix;
        }

    }
}
    

